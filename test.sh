cd pipeline
dir
for i in $(dir);
do 
    if [ "$i" = "__pycache__" ];then
        cd $i
        cd ..
    else 
        cd $i
        echo "============="
        echo "dir = $i"
        echo "============="
        docker build -t "$i" .
        cd ..
    fi
done
